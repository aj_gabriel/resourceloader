package com.spring.boot.example.first.loader;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spring.boot.example.first.loader.model.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.IOException;

@SpringBootApplication
public class LoaderApplication {

    @Autowired
    private ResourceLoader resourceLoader;


    public static void main(String[] args) {

        ApplicationContext appContext =
                new ClassPathXmlApplicationContext();

        ObjectMapper mapper = new ObjectMapper();

        Resource dataResource = appContext.getResource("classpath:data.json");
        try {
            Pair data = mapper.readValue(dataResource.getInputStream(), Pair.class);
            System.out.println("Multiply result:" + data.getFirstValue()*data.getSecondValue());
        } catch (IOException e) {
            e.printStackTrace();
        }

        SpringApplication.run(LoaderApplication.class, args);

    }
}
